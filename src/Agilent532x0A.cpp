/*----- PROTECTED REGION ID(Agilent532x0A.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        Agilent532x0A.cpp
//
// description : C++ source for the Agilent532x0A class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               Agilent532x0A are implemented in this file.
//
// project :     Agilent Frequency counter(timer)
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <Agilent532x0A.h>
#include <Agilent532x0AClass.h>
#include <yat/time/Timer.h>

/*----- PROTECTED REGION END -----*/	//	Agilent532x0A.cpp

/**
 *  Agilent532x0A class description:
 *    This class interfaces a Frequency counter timer for Agilent 53220A and/or 53230A
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  dev_state
//  Status        |  dev_status
//  ClearErrors   |  clear_errors
//================================================================

//================================================================
//  Attributes managed is:
//================================================================
//  frequency  |  Tango::DevDouble	Scalar
//================================================================

namespace Agilent532x0A_ns
{
/*----- PROTECTED REGION ID(Agilent532x0A::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : Agilent532x0A::Agilent532x0A()
 *	Description : Constructors for a Tango device
 *                implementing the classAgilent532x0A
 */
//--------------------------------------------------------
Agilent532x0A::Agilent532x0A(Tango::DeviceClass *cl, string &s)
 : Tango::Device_4Impl(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(Agilent532x0A::constructor_1) ENABLED START -----*/
	init_device();

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::constructor_1
}
//--------------------------------------------------------
Agilent532x0A::Agilent532x0A(Tango::DeviceClass *cl, const char *s)
 : Tango::Device_4Impl(cl, s)
{
	/*----- PROTECTED REGION ID(Agilent532x0A::constructor_2) ENABLED START -----*/
	init_device();

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::constructor_2
}
//--------------------------------------------------------
Agilent532x0A::Agilent532x0A(Tango::DeviceClass *cl, const char *s, const char *d)
 : Tango::Device_4Impl(cl, s, d)
{
	/*----- PROTECTED REGION ID(Agilent532x0A::constructor_3) ENABLED START -----*/
	init_device();

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : Agilent532x0A::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void Agilent532x0A::delete_device()
{
	DEBUG_STREAM << "Agilent532x0A::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(Agilent532x0A::delete_device) ENABLED START -----*/

	//	Delete device allocated objects
  if ( this->_agilentTask )
  {
    //- ask the task to quit
    this->_agilentTask->exit();
    //- !!!!! NEVER TRY TO <delete> a yat4tango::DeviceTask, it commits suicide
    //- upon return of its main function (i.e. entry point)!!!!!!
    this->_agilentTask = 0;
  }

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::delete_device
	delete[] attr_frequency_read;
}

//--------------------------------------------------------
/**
 *	Method      : Agilent532x0A::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void Agilent532x0A::init_device()
{
	DEBUG_STREAM << "Agilent532x0A::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(Agilent532x0A::init_device_before) ENABLED START -----*/
  yat::Timer t;

	//	Initialization before get_device_property() call
	this->_agilentTask = 0;

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::init_device_before

	//	Get the device properties from database
	get_device_property();

	if ( mandatoryNotDefined )
  {
    FATAL_STREAM << "Missing mandatory property \"CommunicationLinkName\"." << std::endl;
    set_state(Tango::FAULT);
    set_status("Missing mandatory property \"CommunicationLinkName\".");
    return;
  }

	attr_frequency_read = new Tango::DevDouble[1];

	/*----- PROTECTED REGION ID(Agilent532x0A::init_device) ENABLED START -----*/
  //- instanciate and start thread
  try
  {
    this->_agilentTask = new Agilent532x0ATask(this, this->communicationLinkName, this->frequencyResolution, this->channelNumber);
    //- set start task timeout (in millisec)
    this->_agilentTask->go(3000);
  }
  catch(...)
  {
    FATAL_STREAM << "Failed to start the monitoring task." << std::endl;
    set_state(Tango::FAULT);
    set_status("Failed to start the monitoring task.");
    return;
  }

  DEBUG_STREAM << "Agilent532x0A::init_device -> INIT DONE in " << t.elapsed_msec() << " ms." << std::endl;

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::init_device
}

//--------------------------------------------------------
/*
* *	Method      : Agilent532x0A::get_device_property()
 *	Description : Read database to initialize property data members.
 */
//--------------------------------------------------------
void Agilent532x0A::get_device_property()
{
	/*----- PROTECTED REGION ID(Agilent532x0A::get_device_property_before) ENABLED START -----*/

	//	Initialize property data members
	communicationLinkName.clear();
	channelNumber = "3";						//- default channel (if not specified)
	frequencyResolution = 0.01;			//- default frequency resolution (if not specified)

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::get_device_property_before

	mandatoryNotDefined = false;

	//	Read device properties from database.
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("CommunicationLinkName"));
	dev_prop.push_back(Tango::DbDatum("ChannelNumber"));
	dev_prop.push_back(Tango::DbDatum("FrequencyResolution"));

	//	is there at least one property to be read ?
	if (dev_prop.size()>0)
	{
		//	Call database and extract values
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);

		//	get instance on Agilent532x0AClass to get class property
		Tango::DbDatum	def_prop, cl_prop;
		Agilent532x0AClass	*ds_class =
			(static_cast<Agilent532x0AClass *>(get_device_class()));
		int	i = -1;

		//	Try to initialize CommunicationLinkName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  communicationLinkName;
		else {
			//	Try to initialize CommunicationLinkName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  communicationLinkName;
		}
		//	And try to extract CommunicationLinkName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  communicationLinkName;
		//	Property StartDsPath is mandatory, check if has been defined in database.
		check_mandatory_property(cl_prop, dev_prop[i]);

		//	Try to initialize ChannelNumber from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  channelNumber;
		else {
			//	Try to initialize ChannelNumber from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  channelNumber;
		}
		//	And try to extract ChannelNumber value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  channelNumber;

		//	Try to initialize FrequencyResolution from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  frequencyResolution;
		else {
			//	Try to initialize FrequencyResolution from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  frequencyResolution;
		}
		//	And try to extract FrequencyResolution value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  frequencyResolution;

	}

	/*----- PROTECTED REGION ID(Agilent532x0A::get_device_property_after) ENABLED START -----*/

	//	Check device property data members init

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::get_device_property_after
}
//--------------------------------------------------------
/**
 *	Method      : Agilent532x0A::check_mandatory_property()
 *	Description : For mandatory properties check if defined in database.
 */
//--------------------------------------------------------
void Agilent532x0A::check_mandatory_property(Tango::DbDatum &class_prop, Tango::DbDatum &dev_prop)
{
	//	Check if all properties are empty
	if (class_prop.is_empty() && dev_prop.is_empty())
	{
		TangoSys_OMemStream	tms;
		tms << endl <<"Property \'" << dev_prop.name;
		if (Tango::Util::instance()->_UseDb==true)
			tms << "\' is mandatory but not defined in database";
		else
			tms << "\' is mandatory but cannot be defined without database";
		string	status(get_status());
		status += tms.str();
		set_status(status);
		mandatoryNotDefined = true;
		/*----- PROTECTED REGION ID(Agilent532x0A::check_mandatory_property) ENABLED START -----*/
		cerr << tms.str() << " for " << device_name << endl;

		/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::check_mandatory_property
	}
}


//--------------------------------------------------------
/**
 *	Method      : Agilent532x0A::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void Agilent532x0A::always_executed_hook()
{
	/*----- PROTECTED REGION ID(Agilent532x0A::always_executed_hook) ENABLED START -----*/

	//	code always executed before all requests

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : Agilent532x0A::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void Agilent532x0A::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "Agilent532x0A::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(Agilent532x0A::read_attr_hardware) ENABLED START -----*/

	//	Add your own code
  static const double HERTZ_TO_MEGAHERTZ = 0.000001;
	//- read back frequency value
	if ( this->_agilentTask )
    *attr_frequency_read = this->_agilentTask->read_frequency() * HERTZ_TO_MEGAHERTZ;

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::read_attr_hardware
}

//--------------------------------------------------------
/**
 *	Read attribute frequency related method
 *	Description: The measured frequency value
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void Agilent532x0A::read_frequency(Tango::Attribute &attr)
{
	DEBUG_STREAM << "Agilent532x0A::read_frequency(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(Agilent532x0A::read_frequency) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_frequency_read);

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::read_frequency
}

//--------------------------------------------------------
/**
 *	Method      : Agilent532x0A::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void Agilent532x0A::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(Agilent532x0A::add_dynamic_attributes) ENABLED START -----*/

	//	Add your own code to create and add dynamic attributes if any

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Command State related method
 *	Description: This command gets the device state (stored in its device_state data member) and returns it to the caller.
 *
 *	@returns Device state
 */
//--------------------------------------------------------
Tango::DevState Agilent532x0A::dev_state()
{
	DEBUG_STREAM << "Agilent532x0A::State()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(Agilent532x0A::dev_state) ENABLED START -----*/

	Tango::DevState	argout = Tango::ON; // replace by your own algorithm

	//	Add your own code
	if ( mandatoryNotDefined )
    argout = Tango::FAULT;
  else if ( !this->_agilentTask )
    argout = Tango::FAULT;
  else
    argout = this->_agilentTask->read_state();

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::dev_state
	set_state(argout);    // Give the state to Tango.
	if (argout!=Tango::ALARM)
		DeviceImpl::dev_state();
	return get_state();  // Return it after Tango management.
}
//--------------------------------------------------------
/**
 *	Command Status related method
 *	Description: This command gets the device status (stored in its device_status data member) and returns it to the caller.
 *
 *	@returns Device status
 */
//--------------------------------------------------------
Tango::ConstDevString Agilent532x0A::dev_status()
{
	DEBUG_STREAM << "Agilent532x0A::Status()  - " << device_name << endl;

	/*----- PROTECTED REGION ID(Agilent532x0A::dev_status) ENABLED START -----*/
	std::string	status = "Device is OK";
	if ( mandatoryNotDefined )
    status = "CommunicationLink property is not well set !";
  else if ( !this->_agilentTask )
    status = "Failed to start the Agilent moitoring task";
  else
    status = this->_agilentTask->read_status();

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::dev_status
	set_status(status);               // Give the status to Tango.
	return DeviceImpl::dev_status();  // Return it.
}
//--------------------------------------------------------
/**
 *	Command ClearErrors related method
 *	Description: This command clears the device errors buffer.
 *
 */
//--------------------------------------------------------
void Agilent532x0A::clear_errors()
{
	DEBUG_STREAM << "Agilent532x0A::ClearErrors()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(Agilent532x0A::clear_errors) ENABLED START -----*/

	//	Add your own code

	/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::clear_errors
}

/*----- PROTECTED REGION ID(Agilent532x0A::namespace_ending) ENABLED START -----*/

/*
//	Additional Methods
std::string Agilent532x0A::get_error()
{
	//- This commands empties the Agilent buffer error list
	static std::string cmd("SYST:ERR?");
	//- send the command only if the previous read has not returned an error
	if ( this->_error.find("No error") != std::string::npos )
		this->_error = this->write_read(cmd);
	return this->_error;
}

double Agilent532x0A::read_frequency_from_hw()
{
	static std::string cmd("READ?");
	std::string response = this->write_read(cmd);
	double value = XString<double>::convertFromString(response);
	return value;
}

std::string Agilent532x0A::write_read(std::string cmd_to_send)
{
	std::string response("");
	//- check if the command is a write only or need or to read back a response (? character at the end of the command)
	if (this->_communicationLink)
	{
		try
		{
			//- check if command returns a response
			if (cmd_to_send.rfind("?") != std::string::npos)
				this->_communicationLink->command_inout("WriteRead", cmd_to_send, response);
			else
				this->_communicationLink->command_in("Write", cmd_to_send);
		}
		catch(Tango::DevFailed &df)
		{
			ERROR_STREAM << device_name << " :\n" << df << std::endl;

			Tango::Except::re_throw_exception(df,
				"COMUNICATION_ERROR",
				"Cannot send/receive data to/from agilent device : caught a DevFailed exception.",
				"Agilent532x0A::write_read");
		}
		catch(...)
		{
			Tango::Except::throw_exception(
				"COMUNICATION_ERROR",
				"Cannot send/receive data to/from agilent device : caught [...] exception.",
				"Agilent532x0A::write_read");
		}
	}

	return response;
}
*/

/*----- PROTECTED REGION END -----*/	//	Agilent532x0A::namespace_ending
} //	namespace
