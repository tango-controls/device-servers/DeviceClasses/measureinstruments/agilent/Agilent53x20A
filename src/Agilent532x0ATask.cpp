// ============================================================================
//
// = CONTEXT
//    TANGO Project - Task to gather Neutron monitor data
//
// = File
//    Agilent532x0ATask.cpp
//
// = AUTHOR
//    X. Elattaoui - SOLEIL
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "Agilent532x0ATask.hpp"
#include <yat/time/Time.h>
#include <Xstring.h>

const size_t PERIODIC_MSG_PERIOD = 3000; //- in ms
const size_t RESPONSE_TMOUT      = 3000; //- in ms

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
const size_t kREAD_FREQUENCY = (yat::FIRST_USER_MSG + 10);

//namespace Agilent532x0A_ns
//{
// ======================================================================
// Agilent532x0ATask::Agilent532x0ATask
// ======================================================================
Agilent532x0ATask::Agilent532x0ATask (Tango::DeviceImpl* host_device, std::string& dsProxyName, double freqResolution, std::string& chNum)
  : yat4tango::DeviceTask(host_device),
    _host_device(host_device),
    _comLink(0),
    _proxyDeviceName(dsProxyName),
    _frequency(0.),
    _freqResolution(freqResolution),
    _channelNumber(chNum),
    _state(Tango::INIT),
    _status("Init on the way ...")
{

}

// ======================================================================
// Agilent532x0ATask::~Agilent532x0ATask
// ======================================================================
Agilent532x0ATask::~Agilent532x0ATask (void)
{

}

// ============================================================================
// Agilent532x0ATask::process_message
// ============================================================================
void Agilent532x0ATask::process_message (yat::Message& _msg) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "Agilent532x0ATask::handle_message::receiving msg " << _msg.to_string() << std::endl;

  //- handle msg
  switch (_msg.type())
  {
  //- THREAD_INIT ----------------------
  case yat::TASK_INIT:
    {
      DEBUG_STREAM << "Agilent532x0ATask::handle_message::THREAD_INIT::thread is starting up" << std::endl;

      //- "initialization" code goes here
      //----------------------------------------------------
      yat::Timer t;

      try
      {
    	//- create proxy
    	this->create_proxy();
        //- Reset the Agilent device
        this->write_read("*RST");
        //- Set the sample count per reading to ONE
        this->write_read("SAMP COUN 1");

        //- Configure de Agilent to measure 352 MHz signal with 0.1 Hz resolution on channel 3 (default channel if not specified in property)
        //- ... the expected cmd is in the form : "CONF:FREQ 352E6, 0.1, (@3)"
        std::string frequencyResolutionStr = XString<double>::convertToString(this->_freqResolution);
        std::string conf_cmd = "CONF:FREQ 352E6, " + frequencyResolutionStr + ", (@" + this->_channelNumber + ")";
        this->write_read(conf_cmd);

        //- configure optional msg handling
        this->enable_timeout_msg(false);
        this->enable_periodic_msg(true);
        this->set_periodic_msg_period(PERIODIC_MSG_PERIOD);
      }
      catch(std::bad_alloc)
      {
        //    this->init_device_done = false;
        FATAL_STREAM << "Agilent532x0A::init_device() : OUT_OF_MEMORY unable to create proxy on communicationLinkName " << std::endl;
        this->delete_proxy();
      }
      catch(...)
      {
        //    this->init_device_done = false;
        FATAL_STREAM << "Agilent532x0A::init_device() : OUT_OF_MEMORY unable to create proxy on communicationLinkName " << std::endl;
        this->delete_proxy();
      }

      INFO_STREAM << "\tAgilent532x0ATask::TASK_INIT finished in " << t.elapsed_msec() << " ms." << std::endl;
    }
    break;
  //- THREAD_EXIT ----------------------
  case yat::TASK_EXIT:
    {
      DEBUG_STREAM << "Agilent532x0ATask::handle_message::THREAD_EXIT::thread is quitting" << std::endl;
      //- "release" code goes here
      //----------------------------------------------------
      try
      {
        this->delete_proxy();
      }
      catch(...)
      {
        //- Noop
      }
    }
    break;
  //- THREAD_PERIODIC ------------------
  case yat::TASK_PERIODIC:
    {
      DEBUG_STREAM << "Agilent532x0ATask::handle_message::handling THREAD_PERIODIC msg" << std::endl;

      //- code relative to the task's periodic job goes here
      //----------------------------------------------------
      yat::Timer t;
      INFO_STREAM << "\n\n\t******Agilent532x0ATask::TASK_PERIODIC ENTERING ..." << std::endl;

      this->update_frequency();

      this->update_state_status();

      INFO_STREAM << "\t******Agilent532x0ATask::TASK_PERIODIC done (with FREQ) in : " << t.elapsed_msec() << " ms.\n" << std::endl;
    }
    break;
  //- THREAD_TIMEOUT -------------------
  case yat::TASK_TIMEOUT:
    {
      //- code relative to the task's tmo handling goes here

      DEBUG_STREAM << "Agilent532x0ATask::handle_message::handling THREAD_TIMEOUT msg" << std::endl;
    }
    break;
  //- CHECK ERROR(S) -------------------
  case kREAD_FREQUENCY:
    {
      //- code relative to the task's tmo handling goes here
      yat::Timer t;

      //this->update_frequency();

      INFO_STREAM << "Agilent532x0ATask::handle_message::handled kREAD_FREQUENCY msg in : " << t.elapsed_msec() << " ms.\n" << std::endl;
    }
    break;
  //- UNHANDLED MSG --------------------
  default:
    FATAL_STREAM << "Agilent532x0ATask::handle_message::unhandled msg type received" << std::endl;
    break;
  }

  DEBUG_STREAM << "Agilent532x0ATask::handle_message::message_handler:msg "
               << _msg.to_string()
               << " successfully handled"
               << std::endl;
}

// ============================================================================
// Agilent532x0ATask::read_frequency
// ============================================================================
double Agilent532x0ATask::read_frequency( )
{
/*  bool waitable = true;
  //- create and post msg
  yat::Message* msg = new yat::Message(kREAD_FREQUENCY, MAX_USER_PRIORITY, waitable);
  if ( !msg )
  {
    ERROR_STREAM << "Agilent532x0ATask::read_frequency-> yat::Message allocation failed." << std::endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "Agilent532x0ATask::read_frequency");
  }

  //- wait till the message is processed !!
  this->wait_msg_handled(msg, RESPONSE_TMOUT);
*/
  yat::AutoMutex<> guard(this->_data_mutex);
  return this->_frequency;
}

// ============================================================================
// Agilent532x0ATask::read_state
// ============================================================================
Tango::DevState Agilent532x0ATask::read_state( )
{
  yat::AutoMutex<> guard(this->_stateStatus_mutex);
  return this->_state;
}

// ============================================================================
// Agilent532x0ATask::read_status
// ============================================================================
std::string Agilent532x0ATask::read_status( )
{
  yat::AutoMutex<> guard(this->_stateStatus_mutex);
  return this->_status;
}

// **************************************************************************
//            update_state_status
//    Read back agilent errors and returns a Tango state/status
//    argin : "SYST:ERR?"
//    argout: no argout
//
// **************************************************************************
void Agilent532x0ATask::update_state_status( )
{
  static std::string stateCmd = "SYST:ERR?";

  { //- enter critical section
    yat::AutoMutex<> guard(this->_stateStatus_mutex);

    //- send command
    this->_status = write_read(stateCmd);

    //- check response
    if ( this->_status.find("No error") != std::string::npos )
    {
      this->_state = Tango::ON;
    }
    else
    {
      this->_state = Tango::ALARM;
    }
  }//- end critical section
}

// **************************************************************************
//            update_frequency
//
//    send 'READ?' command to the agilent
//    argin : str cmd
//    argout : str value
//
// **************************************************************************
void Agilent532x0ATask::update_frequency( )
{
  static std::string readfreqCmd("READ?");
  std::string resp("");

  //- send command
  resp = write_read(readfreqCmd);

    { //- enter critical section
    yat::AutoMutex<> guard(this->_data_mutex);
    this->_frequency = XString<double>::convertFromString(resp);
  }
}

//-----------------------------------------------------------------------
//
//                            INTERNAL METHODS
//
//-----------------------------------------------------------------------
// ============================================================================
// Agilent532x0ATask::create_proxy
// ============================================================================
void Agilent532x0ATask::create_proxy()
{
  if( !this->_comLink )
  {
    try
    {
      if ( !this->_proxyDeviceName.empty() )
      {
        //- Creates the Proxy
        this->_comLink =  new Tango::DeviceProxyHelper(this->_proxyDeviceName,this->_host_device);
        //- Check device is up and running
        this->_comLink->get_device_proxy()->ping();
      }
    }//- TODO error management
    catch(std::bad_alloc& bd)
    {
      FATAL_STREAM << "Agilent532x0ATask::create_proxy() : OUT_OF_MEMORY unable to create proxy on the communication link device " << endl;
      FATAL_STREAM << "Description :\n" << bd.what() << std::endl;
      this->delete_proxy();
    }
    catch(Tango::DevFailed& df)
    {
      FATAL_STREAM << "\tPROXY DevFailed : " << df << std::endl;
      Tango::Except::print_exception(df);
      this->delete_proxy();
    }
    catch(...)
    {
      FATAL_STREAM << "Agilent532x0ATask::create_proxy() : unable to create proxy on the communication link device, caught [...]. " << endl;
      this->delete_proxy();
    }
  }
}

// ============================================================================
// Agilent532x0ATask::delete_proxy
// ============================================================================
void Agilent532x0ATask::delete_proxy()
{
  if ( this->_comLink )
  {
    delete this->_comLink;
    this->_comLink = 0;
  }
}

// **************************************************************************
//            Agilent532x0ATask::write_read
//
//    send specific command to the Agilent and return its response
//    argin : string command
//    argout : string response
//
// **************************************************************************
std::string Agilent532x0ATask::write_read(std::string cmd_to_send)
{
	std::string response("");
	//- check if the command is a write only or need or to read back a response (? character at the end of the command)
	if (this->_comLink)
	{
		try
		{
			//- check if command returns a response
			if (cmd_to_send.rfind("?") != std::string::npos)
				this->_comLink->command_inout("WriteRead", cmd_to_send, response);
			else
				this->_comLink->command_in("Write", cmd_to_send);
		}
		catch(Tango::DevFailed &df)
		{
			ERROR_STREAM << df << std::endl;

			Tango::Except::re_throw_exception(df,
				"COMUNICATION_ERROR",
				"Cannot send/receive data to/from agilent device : caught a DevFailed exception.",
				"Agilent532x0ATask::write_read");
		}
		catch(...)
		{
			Tango::Except::throw_exception(
				"COMUNICATION_ERROR",
				"Cannot send/receive data to/from agilent device : caught [...] exception.",
				"Agilent532x0ATask::write_read");
		}
	}

	return response;
}

//} // namespace
