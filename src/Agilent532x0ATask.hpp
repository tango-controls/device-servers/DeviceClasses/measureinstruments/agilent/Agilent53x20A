// ============================================================================
//
// = CONTEXT
//		Agilent532x0ATask
//
// = File
//		Agilent532x0ATask.hpp
//
// = AUTHOR
//		X. Elattaoui Synchrotron Soleil France
//
// ============================================================================

#ifndef _AGILENT_532x0A_TASK_H_
#define _AGILENT_532x0A_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/threading/Mutex.h>
#include <DeviceProxyHelper.h>

/**
 *  Agilent532x0ATask class description:
 *    This class manages the Agilent 532x0A device. It reads back the frequency, its error(s) and configuration.
 *    Till now, the frequency value needs more than 1s to completed !!
 *    The communication interface used : GPIB.
 *    No commands Start, Stop needed.
 */

// ============================================================================
// class: Agilent532x0ATask
// ============================================================================
class Agilent532x0ATask : public yat4tango::DeviceTask
{
public:
	//- ctor ---------------------------------
  Agilent532x0ATask (Tango::DeviceImpl * host_device, std::string& dsProxyName, double freqResolution, std::string& chNum);

	//- dtor ---------------------------------
	virtual ~Agilent532x0ATask ();

	//- getters ------------------------------
  double read_frequency();

  Tango::DevState read_state();

  std::string read_status();

protected:

  //- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg) throw (Tango::DevFailed);

  //- HW access
  std::string write_read(std::string cmd_to_send);

private:
  void create_proxy();
  void delete_proxy();
  void update_frequency();
  void update_state_status();

  //- communication link proxy
  Tango::DeviceImpl*	 			_host_device;
  Tango::DeviceProxyHelper* _comLink;
  std::string               _proxyDeviceName;

  double          _frequency;
  double          _freqResolution;
  std::string     _channelNumber;
  Tango::DevState _state;
  std::string     _status;

	//- internal mutex
	yat::Mutex	_data_mutex;
	yat::Mutex	_stateStatus_mutex;
};

#endif // _AGILENT_532x0A_TASK_H_
