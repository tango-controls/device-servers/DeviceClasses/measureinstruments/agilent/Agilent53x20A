/*----- PROTECTED REGION ID(Agilent532x0A.h) ENABLED START -----*/
//=============================================================================
//
// file :        Agilent532x0A.h
//
// description : Include file for the Agilent532x0A class
//
// project :     Agilent Frequency counter(timer)
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef Agilent532x0A_H
#define Agilent532x0A_H

#include <tango.h>
#include <DeviceProxyHelper.h>
#include "Agilent532x0ATask.hpp"
/*----- PROTECTED REGION END -----*/  //  Agilent532x0A.h

/**
 *  Agilent532x0A class description:
 *    This class interfaces a Frequency counter timer for Agilent 53220A and/or 53230A
 */

namespace Agilent532x0A_ns
{
/*----- PROTECTED REGION ID(Agilent532x0A::Additional Class Declarations) ENABLED START -----*/

//  Additional Class Declarations

/*----- PROTECTED REGION END -----*/  //  Agilent532x0A::Additional Class Declarations

class Agilent532x0A : public Tango::Device_4Impl
{

  /*----- PROTECTED REGION ID(Agilent532x0A::Data Members) ENABLED START -----*/

//  Add your own data members

  /*----- PROTECTED REGION END -----*/  //  Agilent532x0A::Data Members

//  Device property data members
public:
  //  CommunicationLinkName:  Device which handles the communication protocol (GPIB)
  string  communicationLinkName;
  //  ChannelNumber:  The specied channel to read (default is channel 3)
  string  channelNumber;
  //  FrequencyResolution:  The wanted frequency resolution (default is 0.01)
  Tango::DevDouble  frequencyResolution;

  bool  mandatoryNotDefined;

//  Attribute data members
public:
  Tango::DevDouble  *attr_frequency_read;

//  Constructors and destructors
public:
  /**
   * Constructs a newly device object.
   *
   *  @param cl Class.
   *  @param s  Device Name
   */
  Agilent532x0A(Tango::DeviceClass *cl,string &s);
  /**
   * Constructs a newly device object.
   *
   *  @param cl Class.
   *  @param s  Device Name
   */
  Agilent532x0A(Tango::DeviceClass *cl,const char *s);
  /**
   * Constructs a newly device object.
   *
   *  @param cl Class.
   *  @param s  Device name
   *  @param d  Device description.
   */
  Agilent532x0A(Tango::DeviceClass *cl,const char *s,const char *d);
  /**
   * The device object destructor.
   */
  ~Agilent532x0A() {
    delete_device();
  };


//  Miscellaneous methods
public:
  /*
   *  will be called at device destruction or at init command.
   */
  void delete_device();
  /*
   *  Initialize the device
   */
  virtual void init_device();
  /*
   *  Read the device properties from database
   */
  void get_device_property();
  /*
   *  Always executed method before execution command method.
   */
  virtual void always_executed_hook();

  /*
   *  Check if mandatory property has been set
   */
  void check_mandatory_property(Tango::DbDatum &class_prop, Tango::DbDatum &dev_prop);

//  Attribute methods
public:
  //--------------------------------------------------------
  /*
   *  Method      : Agilent532x0A::read_attr_hardware()
   *  Description : Hardware acquisition for attributes.
   */
  //--------------------------------------------------------
  virtual void read_attr_hardware(vector<long> &attr_list);

  /**
   *  Attribute frequency related methods
   *  Description: The measured frequency value
   *
   *  Data type:  Tango::DevDouble
   *  Attr type:  Scalar
   */
  virtual void read_frequency(Tango::Attribute &attr);
  virtual bool is_frequency_allowed(Tango::AttReqType type);


  //--------------------------------------------------------
  /**
   *  Method      : Agilent532x0A::add_dynamic_attributes()
   *  Description : Add dynamic attributes if any.
   */
  //--------------------------------------------------------
  void add_dynamic_attributes();



//  Command related methods
public:
  /**
   *  Command State related method
   *  Description: This command gets the device state (stored in its device_state data member) and returns it to the caller.
   *
   *  @returns Device state
   */
  virtual Tango::DevState dev_state();
  /**
   *  Command Status related method
   *  Description: This command gets the device status (stored in its device_status data member) and returns it to the caller.
   *
   *  @returns Device status
   */
  virtual Tango::ConstDevString dev_status();
  /**
   *  Command ClearErrors related method
   *  Description: This command clears the device errors buffer.
   *
   */
  virtual void clear_errors();
  virtual bool is_ClearErrors_allowed(const CORBA::Any &any);


  /*----- PROTECTED REGION ID(Agilent532x0A::Additional Method prototypes) ENABLED START -----*/

//  Additional Method prototypes
private :

  //- task
  Agilent532x0ATask* _agilentTask;

  /*----- PROTECTED REGION END -----*/  //  Agilent532x0A::Additional Method prototypes
};

/*----- PROTECTED REGION ID(Agilent532x0A::Additional Classes Definitions) ENABLED START -----*/

//  Additional Classes Definitions

/*----- PROTECTED REGION END -----*/  //  Agilent532x0A::Additional Classes Definitions

} //  End of namespace

#endif   // Agilent532x0A_H
